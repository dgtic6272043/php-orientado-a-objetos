<?php  
// Incluimos los archivos carro,avion,barco y metro que contienen las clases hijas
include_once('..\Clases\ejercicio6\carro.php');
include_once('..\Clases\ejercicio6\avion.php');
include_once('..\Clases\ejercicio6\barco.php');
include_once('..\Clases\ejercicio6\metro.php'); 

// Iniciamos en mensaje como una cadena vacia

$mensaje = '';

// Verificacmos que exista el post, y si sí declaramos un switch

if (!empty($_POST)){
    switch ($_POST['tipo_transporte']) {
    	// Si el post es el caso aero, creamos a jet, como instancia de avion y guardamos su resumen en el mensaje
        case 'aereo':
            $jet1 = new avion('jet', '500', 'gasoleo', '2');
            $mensaje = $jet1->resumenAvion();
            break;
        // Si el post es el caso carro, creamos a carro1, como instancia de carro y guardamos su resumen en el mensaje
        case 'terrestre':
            $carro1 = new carro('carro', '200', 'gasolina', '4');
            $mensaje = $carro1->resumenCarro();
            break;

        // Si el post es el caso maritimo, creamos a bergantin1, como instancia de barco y guardamos su resumen en el mensaje
        case 'maritimo':
            $bergantin1 = new barco('bergantin', '40', '45', '15');
            $mensaje = $bergantin1->resumenBarco();
            break;
        // Si el post es el caso subterraneo, creamos a metro1, como instancia de subterraneo y guardamos su resumen en el mensaje
        case 'subterraneo': 
            $metro1 = new metro('metro linea 3', '3km/hrs', 'diesel', '12');
            $mensaje = $metro1->resumenMetro();
            break;
    }
}
?>

<!DOCTYPE html>
<html>
<head>

	<link rel="stylesheet" href="../css/bootstrap.min.css">
	<link rel="stylesheet" href="../css/bootstrap-grid.css">
	<script type="text/javascript" src="../js/bootstrap.min.js"></script>
	<script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
	<title>
		Indice
	</title>
</head>
<body>

	<div class="container" style="margin-top: 4em">
	
	<header> <h1>Los transportes - Garcia Rodriguez Luis Armando</h1></header><br>
	<form method="post">
		

					 <div class="form-group">
				 		<label for="CajaTexto1">Tipo de transporte:</label>
						<select class="form-control" name="tipo_transporte" id="CajaTexto1">
							<option value='aereo' >Aereo</option>
							<option value='terrestre' >Terrestre</option>
							<option value='maritimo' >Maritimo</option>
							<option value='subterraneo' >Subterraneo</option>
						</select>
					</div>

					
			
		<button class="btn btn-primary" type="submit" >Enviar</button>
		<a class="btn btn-link offset-md-8 offset-lg-9 offset-6" href="../index.php">Regresar</a>
	</form>

	</div>
	<div class="container mt-5">
		<h2>Respuesta del servidor</h2>
		<table class="table">
			<thead>
		      <tr>
		      	 <th>Transporte</th>
		      </tr>
		    </thead>
		    <tbody>
			<?= $mensaje; ?>

			</tbody>
		</table>

    </div>



</body>
</html>