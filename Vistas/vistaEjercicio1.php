<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/bootstrap-grid.css">
    <script type="text/javascript" src="../js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
    <title>Indice</title>
</head>
<body>

<!-- Hacemos una inclusion de los archivos php carro1.php y moto1.php en el html -->

    <?php include '..\Clases\ejercicio1\carro1.php'; ?> 
    <?php include '..\Clases\ejercicio1\moto1.php'; ?>

    <!-- Creamos un input para agregar el mensaje del servidor para el carro -->

    <input type="text" class="form-control" value="<?php echo $mensajeServidor; ?>" readonly>

     <!-- Creamos un input para agregar el mensaje del servidor para la moto -->
    <input type="text" class="form-control" value="<?php echo $mensajeServidorMoto; ?>" readonly>

    <div class="container" style="margin-top: 4em">
        <header><h1>Ejercicio1-Carro y Moto - García Rodriguez Luis Armando </h1></header><br>
        <form method="post">

            <!--Div con input para el atributo color del carro-->
            <div class="form-group row">
                <label class="col-sm-3" for="CajaTexto1">Elige el color del carro:</label>
                <div class="col-sm-4">
                    <input class="form-control" type="color" name="color" id="CajaTexto1">
                </div>
            </div>

            <!--Div con input para el atributo marca del carro-->

            <div class="form-group row">
                <label class="col-sm-3" for="marcaCarro">Escribe la marca del carro:</label>
                <div class="col-sm-4">
                    <input class="form-control" type="text" name="marcaCarro" id="marcaCarro">
                </div>
            </div>

        <!--Div con input para el atributo modelo del carro-->

            <div class="form-group row">
                <label class="col-sm-3" for="modeloCarro">Escribe el modelo del carro:</label>
                <div class="col-sm-4">
                    <input class="form-control" type="text" name="modeloCarro" id="modeloCarro">
                </div>
            </div>
            
            <!--Div con input para el atributo color de la moto-->

            <div class="form-group row">
                <label class="col-sm-3" for="colorMoto">Elige el color de la moto:</label>
                <div class="col-sm-4">
                    <input class="form-control" type="color" name="colorMoto" id="colorMoto">
                </div>
            </div>

            <!--Div con input para el atributo marca de la moto-->

            <div class="form-group row">
                <label class="col-sm-3" for="marcaMoto">Escribe la marca de la moto:</label>
                <div class="col-sm-4">
                    <input class="form-control" type="text" name="marcaMoto" id="marcaMoto">
                </div>
            </div>

            <!--Div con input para el atributo modelo de la moto-->

            <div class="form-group row">
                <label class="col-sm-3" for="modeloMoto">Escribe el modelo de la moto:</label>
                <div class="col-sm-4">
                    <input class="form-control" type="text" name="modeloMoto" id="modeloMoto">
                </div>
            </div>

            <!--Boton de envio-->

            <button class="btn btn-primary" type="submit">Enviar</button>
            <a class="btn btn-link offset-md-8 offset-lg-9 offset-6" href="../index.php">Regresar</a>
        </form>
    </div>

</body>
</html>

