<?php
// Incluimos el archivo que tiene el generador de contraseña
include_once('../clases/ejercicio5/GeneradorContrasena.php');

// Captamos la salida del destructor

ob_start();
$mensaje = '';

if (!empty($_POST)) {
    // Creamos una instancia de la clase GeneradorContrasena
    $generador = new GeneradorContrasena();
    // Obtenemos el mensaje para mostrar una saludo y el nombre
    $mensaje = 'Hola, ' . htmlspecialchars($_POST['nombre']);
}
// Guardamos la salida del destructor
$mensaje_destructor = ob_get_clean();
?>
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/bootstrap-grid.css">
    <script type="text/javascript" src="../js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
    <title>Indice</title>
</head>
<body>
    
    <div class="container" style="margin-top: 4em">
    <header> <h1>Ejercicio3 - Recoge tu constraseña - García Rodriguez Luis Armando</h1></header><br>
    <form method="post">
        <div class="form-group">
            <label for="CajaTexto1">Escribe tu nombre:</label>
            <input class="form-control" type="text" name="nombre" id="CajaTexto1">
        </div>
        <button class="btn btn-primary" type="submit">enviar</button>
        <a class="btn btn-link offset-md-8 offset-lg-9 offset-6" href="../index.php">Regresar</a>
    </form>
    <!--Mostramos el mensaje y el mensaje del destructur, verificando con un if que exista-->
    <?php if (!empty($mensaje)): ?>
        <div class="alert alert-success" role="alert">
            <?= $mensaje; ?>
            <?= $mensaje_destructor; ?>
        </div>
    <?php endif; ?>

    </div>
</body>
</html>
