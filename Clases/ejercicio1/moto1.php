<?php

//Declaracion de la clase moto

class Moto {
    public $marca; // Atributo marca
    public $modelo; // Atributo modelo
    public $color; // Atributo color

// Aplicamos el constructor para inicializar los atributos
    public function __construct($marca = '', $modelo = '', $color = '') {
        $this->marca = $marca;
        $this->modelo = $modelo;
        $this->color = $color;
    }
}

//Inicializamos el mensaje del servidor en una cadena vacia

$mensajeServidorMoto = '';

// Agregamos un if para validar que si se hayan enviado los datos de moto

if (!empty($_POST) && isset($_POST['colorMoto'], $_POST['marcaMoto'], $_POST['modeloMoto'])) {

    // Si sí, modificamos el mensaje del servidor para encadenar las atributos al mensaje
    $mensajeServidorMoto = 'Has elegido una moto con marca: ' . $_POST['marcaMoto'] . ', modelo: ' . $_POST['modeloMoto'] . ' y color: ' . $_POST['colorMoto'];
}


?>