<?php
//creación de la clase carro
class Carro2 {
    public $color; // Atributo color
    public $modelo; // Atributo modelo
    private $estado_verificacion; // atributo privadp para el estado de verificación

    // Verificamos el estado de circulación segun el año de fabricación con este metodo
    public function verificacion($anio_fabricacion) {
        $anio = (int)substr($anio_fabricacion, 0, 4); // Toma el año del valor del input

        if ($anio < 1990) {
            $this->estado_verificacion = 'No';
        } elseif ($anio >= 1990 && $anio <= 2010) {
            $this->estado_verificacion = 'Revisión';
        } else {
            $this->estado_verificacion = 'Sí';
        }
    }

    // Otro método para obtener el estado de verificación
    public function getEstadoVerificacion() {
        return $this->estado_verificacion;
    }
}


//Creación de Carro1 como instancia a la clase Carro
$Carro1 = new Carro2();

if (!empty($_POST)) {
    $Carro1->color = $_POST['color'];
    $Carro1->modelo = $_POST['modelo'];
    // Coneste if nos aseguramos de que se haya enviado el año de fabricación antes de llamar al método
    if (isset($_POST['anio_fabricacion'])) {
        $Carro1->verificacion($_POST['anio_fabricacion']);
    }
}






