<?php  

// Creacion de la clase GeneradosContrasena
class GeneradorContrasena {
    private $contrasena; //Atributo privado contrasena

    // Alicamos un constructor que genera la contraseña
    public function __construct() {
        $this->contrasena = $this->generarContrasena();
    }

    // Cre amos un método para generar una contraseña de 4 letras 
    private function generarContrasena() {
        $caracteres = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $contrasena = '';
        // Creamos un for que itere de 0 a 4 para los cuatro caracteres de la constraseña y generamos un número aleatorio entre 0 y la longitud de la cadena menos uno.

        for ($i = 0; $i < 4; $i++) {
            $contrasena .= $caracteres[rand(0, strlen($caracteres) - 1)];
        }
        return $contrasena;
    }

    // Hacemos un método para obtener la contraseña
    public function obtenerContrasena() {
        return $this->contrasena;
    }

    // Aplicamos el destructor para que muestre la contraseña
    public function __destruct() {
        echo 'Tu contraseña es: ' . $this->contrasena;
    }
}
?>


