<?php  

// incuimos el archivo del que vamos a heredar
include_once('transporte.php');

//Heredamos de la clase transporte a barco

class barco extends transporte {
    private $calado;

    //Sobreescribimos el constructor para agregar el calado
    public function __construct($nom,$vel,$com,$cal){
        parent::__construct($nom,$vel,$com);
        $this->calado=$cal;
    }

    // Creamos el metodo resumenBarco
    public function resumenBarco(){
        $mensaje=parent::crear_ficha();
        $mensaje.='<tr>
                    <td>Calado:</td>
                    <td>'. $this->calado.'</td>                
                </tr>';
        return $mensaje;
    }
}
?>
